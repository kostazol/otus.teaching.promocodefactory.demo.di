﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesBestPracticesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;
        private readonly IEmployeeMapper _employeeMapper;

        public EmployeesBestPracticesController(IRepository<Employee> employeeRepository, 
            IRepository<Role> rolesRepository, IEmployeeMapper employeeMapper)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
            _employeeMapper = employeeMapper;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        
        /// <summary>
        /// Создать сотрудника
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateEmployeeAsync(CreateOrEditEmployeeRequest model)
        {
            // Стараемся не писать маппинг прямо в контроллерах/сервисах, большая часть кода - это маппинг данных, 
            // если не используем конструкторы или классы-мапперы, фабрики, то будут проблемы, так как теряется логика
            // консистентности операций создания и изменения объектов, код легко плодиться и подвержен ошибкам
            // Не пишем маппинг прямо в контроллерах/сервисах
            // Субъективно 50%> ошибок - это из-за нарушения инкапсуляции создания объектов
            
            var roles  = await _rolesRepository.GetByCondition(x => 
                model.RoleNames.Contains(x.Name)) as List<Role>;
            
            //Нарушаем инкапсуляцию создания объекта
            // Employee employee = new Employee()
            // {
            //     Id = Guid.NewGuid(),
            //     FirstName = model.FirstName,
            //     LastName = model.LastName,
            //     Email = model.Email,
            //     AppliedPromocodesCount = model.AppliedPromocodesCount,
            //     Roles = await _rolesRepository
            //         .GetByCondition(x => model.RoleNames.Contains(x.Name)) as List<Role>
            // };
            
            //TODO: Используем  маппер вместо этого

            var employee = await _employeeMapper.MapFromModelAsync(model);
            
            try
            {
                await _employeeRepository.CreateAsync(employee);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
            return CreatedAtAction(nameof(GetEmployeeByIdAsync), new {id = employee.Id}, null);
        }

        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployeeAsync(Guid id, CreateOrEditEmployeeRequest model)
        {
            Employee employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
            {
                return BadRequest();
            }
            
            // Стараемся не писать маппинг прямо в контроллерах/сервисах, большая часть кода - это маппинг данных, 
            // если не используем конструкторы или классы-мапперы, фабрики, то будут проблемы, так как теряется логика
            // консистентности операций создания и изменения объектов, код легко плодиться и подвержен ошибкам
            // Не пишем маппинг прямо в контроллерах/сервисах
            // Субъективно 50%> ошибок - это из-за нарушения инкапсуляции создания/изменения объектов
            
            employee.FirstName = model.FirstName;
            employee.LastName = model.LastName;
            employee.AppliedPromocodesCount = model.AppliedPromocodesCount;
            employee.Email = model.Email;
            employee.Roles = await _rolesRepository.GetByCondition(x => 
                model.RoleNames.Contains(x.Name)) as List<Role>;
            
            //TODO: Используем Mapper вместо этого

            try
            {
                await _employeeRepository.UpdateAsync(employee);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {

            Employee toDelete = await _employeeRepository.GetByIdAsync(id);

            if (toDelete == null)
            {
                return NotFound();
            }
            try
            {
                await _employeeRepository.DeleteAsync(toDelete);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}